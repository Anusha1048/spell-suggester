Algorithm:-

correction(word): Returns the most probable result for the misspelled word

candidates(word): Returns a set of possible candidates for the misspelled 

wordknown([words]): Returns those words that are in the word frequency list

unknown([words]): Returns those words that are not in the frequency list

word_probability(word): The frequency of the given word out of all words in the frequency list

edit_distance1(word): Returns a set of all strings at a Levenshtein Distance of one based on the alphabet of the selected language

Generate all possible terms with an edit distance (deletes + transposes + replaces + inserts) from the query term and search them in the dictionary. For a word of length n, an alphabet size a, an edit distance d=1, there will be n deletions, n-1 transpositions, a*n alterations, and a*(n+1) insertions, for a total of 2n+2an+a-1 terms at search time.

edit_distance_2(word): Returns a set of all strings at a Levenshtein Distance of two based on the alphabet of the selected language