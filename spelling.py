from __future__ import print_function
import re
import operator
import collections
from collections import Counter
from collections import defaultdict
import tldextract
import re
from googlesearch import search
try:
    from googlesearch import search
except ImportError:
    print("No module named 'google' found")

def words(text): return re.findall(r'\w+', text.lower())

WORDS = Counter(words(open('frequency_dictionary_en_82_765.txt').read()))
# print(WORDS)

edit_table = collections.defaultdict(lambda: 0)
# with open('count_1edit.txt', 'r') as f:
#     for line in f:
#         contents = line.split('\t')
#         edit_table[contents[0]] = int(contents[1])
#     # print(edit_table)
def edit_count(s1, s2):
    """Returns how many times substring s1 is edited as s2."""
    return edit_table[s1 + "|" + s2]
# print(edit_count("pm","mp"))
def P(words=[], N=sum(WORDS.values())):
    "Probability of `word`."
    # print(word)
    # print(WORDS[word]/N)
    list_of_prob=[]
    for p_word in words:
        list_of_prob.append(WORDS[p_word]/N)
    return list_of_prob

def correction(word):
    "Most probable spelling correction for word."

    # words=[candidates((word))]
    # print(words)
    return candidates(word)

def candidates(word):
    "Generate possible spelling corrections for word."
    print("***")
    # print(known([word]) or known(edits1(word)) or known(edits2(word)))
    # # return  (known([word]) or known(edits1(word)) or known(edits2(word)) or [word])
    # if(known([word]))
    # print(suggestion)
    suggestions=(known([word]))
    print("word in")
    print(suggestions)
    org_list=[]
    if(suggestions):
        edit_words=edits_for_org(word)
        # print("oorg")
        # print(edit_words)
        print(edit_words)

        for i in range(len(edit_words)):
            # print(edit_words[i])
            query=edit_words[i]

            print(query)
            if query not in org_list:
                for j in search(query, tld="co.in", num=1, stop=1, pause=2):
                    print(j)
                    # org_list.append(query)
                    # result = re.sub(r'(.*://)?([^/?]+).*', '\g<1>\g<2>', j)
                    # print(result)
                    ext = tldextract.extract(j)
                    # print("subdomain")
                    # print(ext.subdomain)
                    # print("domain")
                    # print(ext.domain)
                    # print("suffix")
                    # print(ext.suffix)
                    if (edit_words[i] == ext.domain):
                        org_list.append(edit_words[i])
                    if (edit_words[i] == ext.subdomain):
                        org_list.append(edit_words[i])
                print("$$$")
                print(org_list)
    if(not suggestions):
        print("T")
        suggestions=(known(edits1(word)))
    if(not suggestions):
        print("2T")
        suggestions=known(edits2(word))
    print(suggestions)
    return 1

def known(words):
    "The subset of `words` that appear in the dictionary of WORDS."
    # print("known")
    # print(set(w for w in words if w in WORDS))
    print("Final sugggestions")
    # print(list(w for w in words if w in WORDS))
    return list(w for w in words if w in WORDS)

def edits1(word):
    "All edits that are one edit away from `word`."
    alphabet    = 'abcdefghijklmnopqrstuvwxyz'
    # splits     = [(word[:i], word[i:])    for i in range(len(word) + 1)]
    # deletes    = [L + R[1:]               for L, R in splits if R]
    # transposes = [L + R[1] + R[0] + R[2:] for L, R in splits if len(R)>1]
    # replaces   = [L + c + R[1:]           for L, R in splits if R for c in letters]
    # inserts    = [L + c + R               for L, R in splits for c in letters]
    # print("edits1")
    # print(set(splits))
    # print(set(deletes))
    # print(set(transposes))
    # print(set(replaces))
    # print(set(inserts))
    # print(set(deletes + transposes + replaces + inserts))
    s = [(word[:i], word[i:]) for i in range(len(word) + 1)]
    counts = collections.defaultdict(lambda: 0)
    # deletes
    deletes=[]
    transposes=[]
    replaces=[]
    inserts=[]
    for a, b in s:
        if b and a + b[1:] in WORDS:
            tail = ''
            if len(a) > 0:
                tail = a[-1]
            original = tail + b[0]
            replacement = tail
            count = edit_count(original, replacement)
            if count:
                counts[a + b[1:]] += count
        else:
            if b:
               deletes.append(a+b[1:])

    # transposes
    for a, b in s:
        if len(b) > 1 and a + b[1] + b[0] + b[2:] in WORDS:
            # new word is a + b[1] + b[0] + b[2:]
            # edit is b[0]b[1] -> b[1]b[0]
            original = b[0] + b[1]
            replacement = b[1] + b[0]
            count = edit_count(original, replacement)
            if count:
                counts[a + b[1] + b[0] + b[2:]] += count
        else:
            if len(b)>1:
                transposes.append(a+b[1]+b[0]+b[2:])
    # replaces
    for a, b in s:
        if b:
            for c in alphabet:
                if a + c + b[1:] in WORDS:
                    # new word is a + c + b[1:].
                    original = b[0]
                    replacement = c
                    count = edit_count(original, replacement)
                    if count:
                        counts[a + c + b[1:]] += count
        else:
            replaces.append(a+c+b[1:])

    # inserts
    for a, b in s:
        for c in alphabet:
            if a + c + b in WORDS:
                # new word is a + c + b.
                tail = ''
                if len(a) > 0:
                    tail = a[-1]
                original = tail
                replacement = tail + c
                count = edit_count(original, replacement)
                if count:
                    counts[a + c + b] += count
            else:
                inserts.append(a+c+b)

    # normalize counts. sum over them all, divide each entry by sum.
    total = 0.0
    for a, b in counts.iteritems():
        total += b
    # self count
    selfCount = max(9 * total, 1)
    counts[word] = selfCount
    total += selfCount
    probs = {}
    if (total != 0.0):
        for a, b in counts.iteritems():
            probs[a] = float(b) / total
    if counts:

        # return set(probs.keys())
        p_words=P(probs.keys())

        # print("***((")
        # print(p_words)
        # print(probs.values())
        p_k_given_words=[probs.values()]
        conditional_dict={}
        i=0
        for key,value in probs.iteritems():
            # if(p_words[i]>1):
            conditional_dict[key]=value*p_words[i]*pow(10,9)
            # else:
            #     conditional_dict[key] = value*pow(10,9)
            i=i+1
        # print(conditional_dict)
        sorted_d = dict(sorted(conditional_dict.items(), key=operator.itemgetter(1), reverse=True))
        # print(sorted_d)
        return(sorted_d.keys())




    else:
        # print("%%%")
        # print(set(deletes + transposes + replaces + inserts))
        return set(deletes + transposes + replaces + inserts)

def edits2(word):
    "All edits that are two edits away from `word`."
    # print("edits2")
    # print((e2 for e1 in edits1(word) for e2 in edits1(e1)))
    return (e2 for e1 in edits1(word) for e2 in edits1(e1))
def edits_for_org(word):
    "All edits that are one edit away from `word`."
    alphabet    = 'abcdefghijklmnopqrstuvwxyz'
    # splits     = [(word[:i], word[i:])    for i in range(len(word) + 1)]
    # deletes    = [L + R[1:]               for L, R in splits if R]
    # transposes = [L + R[1] + R[0] + R[2:] for L, R in splits if len(R)>1]
    # replaces   = [L + c + R[1:]           for L, R in splits if R for c in letters]
    # inserts    = [L + c + R               for L, R in splits for c in letters]
    # print("edits1")
    # print(set(splits))
    # print(set(deletes))
    # print(set(transposes))
    # print(set(replaces))
    # print(set(inserts))
    # print(set(deletes + transposes + replaces + inserts))
    s = [(word[:i], word[i:]) for i in range(len(word) + 1)]
    counts = collections.defaultdict(lambda: 0)
    # deletes
    deletes=[]
    transposes=[]
    replaces=[]
    inserts=[]
    dict_words=[]
    for a, b in s:
        if b and a + b[1:] in WORDS:
            dict_words.append(a+b[1:])
        else:
            if b:
               deletes.append(a+b[1:])

    # transposes
    for a, b in s:
        if len(b) > 1 and a + b[1] + b[0] + b[2:] in WORDS:
            dict_words.append(a+b[1]+b[0]+b[2:])
        else:
            if len(b)>1:
                transposes.append(a+b[1]+b[0]+b[2:])
    # replaces
    for a, b in s:
        if b:
            for c in alphabet:
                if a + c + b[1:] in WORDS:
                    dict_words.append(a+c+b[1:])
        else:
            replaces.append(a+c+b[1:])

    # inserts
    for a, b in s:
        for c in alphabet:
            if a + c + b in WORDS:
               dict_words.append(a+c+b)
            else:
                inserts.append(a+c+b)
    # print(set(deletes))
    return deletes + transposes + replaces + inserts

# word=input()
print(correction("car"))




























